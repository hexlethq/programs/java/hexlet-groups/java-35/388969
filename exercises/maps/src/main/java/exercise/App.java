package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
public class App {
    public static Map<String, Integer> getWordCount(String phrase) {
        Map<String, Integer> checkPhrase = new HashMap<>();
        if (!phrase.isEmpty()) {
            String[] words = phrase.split(" ");
            for (String word : words) {
                if (checkPhrase.containsKey(word)) {
                    int howMuchRepeat = checkPhrase.get(word);
                    checkPhrase.put(word, howMuchRepeat + 1);
                } else {
                    checkPhrase.put(word, 1);
                }
            }
        }
        return checkPhrase;
    }
    public static String toString(Map<String, Integer> wordsAndQuantity) {
        String toShow = "{}";
        if (!wordsAndQuantity.isEmpty()) {
            toShow = "{";
            for (Map.Entry<String, Integer> parts : wordsAndQuantity.entrySet()) {
                toShow += "\n" + ("  " + parts.getKey() + ": " + parts.getValue());
            }
            toShow += "\n}";
        }
        return toShow;
    }
}
//END
