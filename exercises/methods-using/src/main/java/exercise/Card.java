package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        String stars = "*";
       for (int i = 1; i < starsCount; i++) {
           stars += "*";
       }
        System.out.println(stars + cardNumber.substring(12));
        // END
    }
}
