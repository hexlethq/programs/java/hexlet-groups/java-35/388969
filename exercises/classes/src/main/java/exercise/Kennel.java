package exercise;

import java.util.Arrays;


// BEGIN
class Kennel {
    private static String[][] allDogs;

    public static void addPuppy(String[] onePuppy) {
        int count = allDogs != null ? allDogs.length : 0;
        String[][] newAllDogs = new String[count + 1][];
        System.arraycopy(allDogs, 0, newAllDogs, 0, count);
        newAllDogs[count] = new String[2];
        allDogs = newAllDogs;
        allDogs[count][0] = onePuppy[0];
        allDogs[count][1] = onePuppy[1];
    }

    public static void addSomePuppies(String[][] somePuppies) {
        for (String[] next: somePuppies) {
            addPuppy(next);
        }
    }

    public static int getPuppyCount() {
        return allDogs.length;
    }

    public static boolean isContainPuppy(String name) {
        for (String[] x: allDogs) {
            if (x[0].equals(name)) {
                return true;
            }
        }
        return false;
    }
    public static String[][] getAllPuppies() {
        String[][] res = new String[allDogs.length][];
        for (int i = 0; i < allDogs.length; i++) {
            res[i] = allDogs[i].clone();
        }
        return res;
    }

    public static String[] getNamesByBreed(String breed) {
        int find = 0;
        for (String[] k: allDogs) {
            if (k[1].equals(breed)) {
                find++;
            }
        }
        String[] res = new String[find];
        int number = 0;
        for (String[] k: allDogs) {
            if (k[1].equals(breed)) {
                res[number] = k[0];
                number++;
            }
        }
        return res;
    }

    public static void resetKennel() {
        allDogs = new String[0][];
        }
}
// END
