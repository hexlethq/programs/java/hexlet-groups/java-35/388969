package exercise;

import java.lang.annotation.Annotation;
import java.lang.reflect.Proxy;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// BEGIN
@Component
public class CustomBeanPostProcessor implements BeanPostProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomBeanPostProcessor.class);
    private Map<Object, String> logMap = new HashMap<>();

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        for (Annotation annotation: bean.getClass().getAnnotations()) {
            if (annotation instanceof Inspect inspect) {
                logMap.put(bean, inspect.level());
            }
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if(logMap.containsKey(bean)) {
            return Proxy.newProxyInstance(
                    bean.getClass().getClassLoader(),
                    bean.getClass().getInterfaces(),
                    (proxy, method, args) -> {
                        String log = "Was called method: %s() with arguments: %s";
                        if(logMap.get(bean).equals("info")) {
                            LOGGER.info(log.formatted(method.getName(), Arrays.toString(args)));
                        } else {
                            LOGGER.debug(log.formatted(method.getName(), Arrays.toString(args)));
                        }
                        return method.invoke(bean, args);
                    });
        }
        return bean;
    }
}
// END
