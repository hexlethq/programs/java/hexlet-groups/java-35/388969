package exercise;// BEGIN


import exercise.geometry.Segment;

public class App {

    public static double[] getMidpointOfSegment(double[][] segment) {
        double[] pointMid = new double[2];
        double[] pointBegin = Segment.getBeginPoint(segment);
        double[] pointEnd = Segment.getEndPoint(segment);
        pointMid[0] = (pointBegin[0] + pointEnd[0]) / 2;
        pointMid[1] = (pointBegin[1] + pointEnd[1]) / 2;
        return pointMid;
    }

    public static double[][] reverse(double[][] segment) {
        double[] beginPoint = Segment.getBeginPoint(segment);
        double[] endPoint = Segment.getEndPoint(segment);
        double[][] res = Segment.makeSegment(endPoint, beginPoint);
        return res;
    }
}
// END
