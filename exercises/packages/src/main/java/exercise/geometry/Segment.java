package exercise.geometry;// BEGIN


public class Segment {


    public static double[][] makeSegment(double[] point1, double[] point2) {
        double[][] segment = new double[2][];
        for (double i: point1) {
            segment[0] = point1;
        }
        for (double i: point2) {
            segment[1] = point2;
        }
        return segment;
    }

    public static double[] getBeginPoint(double[][] segment) {
        double[] beginOfSegment = new double[2];
        System.arraycopy(segment[0], 0, beginOfSegment, 0, segment.length);
        return beginOfSegment;
    }

    public static double[] getEndPoint(double[][] segment) {
        double[] endOfSegment = new double[2];
        System.arraycopy(segment[1], 0, endOfSegment, 0, segment.length);
        return endOfSegment;
    }
}

// END
