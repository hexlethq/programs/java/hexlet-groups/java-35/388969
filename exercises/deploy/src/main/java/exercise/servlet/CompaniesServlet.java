package exercise.servlet;

import exercise.Data;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import static exercise.Data.getCompanies;

public class CompaniesServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        // BEGIN
        List<String> companies = Data.getCompanies();
        PrintWriter pw = response.getWriter();
        String queryString = request.getQueryString();
        String needSearch = request.getParameter("search");

        if (queryString == null || needSearch.isBlank()) {
            companies.forEach(pw::println);
        } else if (!needSearch.isBlank()) {
            List<String> filterCompanies = new LinkedList<>();

            filterCompanies = companies.stream()
                    .filter(company -> company.contains(needSearch))
                    .collect(Collectors.toList());

            if (filterCompanies.isEmpty()) {
                pw.print("Companies not found");
            } else {
                filterCompanies.forEach(pw::println);
            }
        }
    }
        // END
}
