package exercise.controller;

import exercise.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import exercise.service.UserService;


@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UserService userService;

    @GetMapping(path = "")
    public Flux<User> getUsers() {
        return userService.findAll();
    }

    // BEGIN
    @GetMapping(path = "/{userId}")
    public Mono<User> getUser(@PathVariable("userId") int userId) {
        return userService.getUser(userId);
    }

    @PostMapping(path = "")
    public Mono<User> createUser(@RequestBody User user) {
        return userService.createUser(user);
    }

    @PatchMapping(path = "/{userId}")
    public Mono<User> updateUser(@PathVariable int userId, @RequestBody User user) {
        return userService.update(userId, user);
    }

    @DeleteMapping(path = "/{userId}")
    public Mono<Void> deleteUser(@PathVariable int userId) {
        return userService.deleteUser(userId);
    }
    // END
}
