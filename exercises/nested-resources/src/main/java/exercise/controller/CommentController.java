package exercise.controller;

import exercise.model.Comment;
import exercise.repository.CommentRepository;
import exercise.model.Post;
import exercise.repository.PostRepository;
import exercise.ResourceNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PatchMapping;

import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
@RequestMapping("/posts")
public class CommentController {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private PostRepository postRepository;

    // BEGIN
    @GetMapping("/{postId}/comments")
    public Iterable<Comment> allComments(@PathVariable long postId) {
        return commentRepository.findAllByPostId(postId);
    }

    @GetMapping("/{postId}/comments/{commentId}")
    public Comment CommentById(@PathVariable long postId, @PathVariable long commentId) {
        return commentRepository.findCommentByIdAndPostId(commentId, postId)
                .orElseThrow(() -> new ResourceNotFoundException("comment does not exist"));
    }

    @PostMapping("/{postId}/comments")
    public Iterable<Comment> createComment(@PathVariable long postId,
                                           @RequestBody Comment comment) {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new ResourceNotFoundException("post does not exist"));

        comment.setPost(post);
        commentRepository.save(comment);

        return commentRepository.findAllByPostId(postId);
    }

    @PatchMapping("/{postId}/comments/{commentId}")
    public void createComment(@PathVariable long postId,
                              @PathVariable long commentId,
                              @RequestBody Comment inputComment) {
        Comment comment = commentRepository.findCommentByIdAndPostId(commentId, postId)
                .orElseThrow(() -> new ResourceNotFoundException("comment does not exist"));

        comment.setContent(inputComment.getContent());
        commentRepository.save(comment);
    }

    @DeleteMapping("/{postId}/comments/{commentId}")
    public void deleteComment(@PathVariable long postId,
                              @PathVariable long commentId) {
        Comment comment = commentRepository.findCommentByIdAndPostId(commentId, postId)
                .orElseThrow(() -> new ResourceNotFoundException("comment does not exist"));

        commentRepository.delete(comment);
    }
    // END
}
