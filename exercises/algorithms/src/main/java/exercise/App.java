package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int[] sort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            boolean isSorted = false;
            for (int k = 0; k < arr.length -  i; k++) {
                if (arr[k] > arr[k + 1]) {
                    arr[k] = arr[k] + arr[k + 1];
                    arr[k+1] = arr[k] - arr[k + 1];
                    arr[k] = arr[k] - arr[k + 1];
                    isSorted = true;
                }
            }
            if (!isSorted) {
                return arr;
            }
        }
        return arr;
        // END
    }
}
