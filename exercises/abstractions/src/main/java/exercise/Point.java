package exercise;

class Point {
    // BEGIN
    public static int[] makePoint(int x, int y) {
        int[] point = new int[2];
        point[0] = x;
        point[1] = y;
        return point;
    }

    public static int getX(int[] arr) {
        return arr[0];
    }

    public static int getY(int[] arr) {
        return arr[1];
    }

    public static String pointToString(int[] arr) {
        return ("(" + arr[0] + ", " + arr[1] + ")");
    }

    public static int getQuadrant(int[] arr) {
        int res = 0;
        if (arr[0] > 0 && arr[1] > 0) {
            res = 1;
        }
        if (arr[0] < 0 && arr[1] > 0) {
            res = 2;
        }
        if (arr[0] < 0 && arr[1] < 0) {
            res = 3;
        }
        if (arr[0] > 0 && arr[1] < 0) {
            res = 4;
        }
        return res;
    }
    // END
}
