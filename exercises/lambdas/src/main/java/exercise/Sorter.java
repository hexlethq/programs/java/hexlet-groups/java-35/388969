package exercise;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.List;
import java.time.LocalDate;
import java.util.stream.Collectors;

// BEGIN
class Sorter {
    public static List<String> takeOldestMens(List<Map<String, String>> allGuys) {
        return allGuys.stream()
                .filter(guy -> guy.get("gender").equals("male"))
                .sorted(Comparator.comparing(birthday -> birthday.get("birthday")))
                .map(name -> name.get("name"))
                .collect(Collectors.toList());
    }
}
// END
