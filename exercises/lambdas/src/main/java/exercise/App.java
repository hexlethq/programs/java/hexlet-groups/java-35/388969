package exercise;

import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
class App {
    public static String[][] enlargeArrayImage(String[][] matrix) {
        return Arrays.stream(matrix)
                .flatMap(line -> Stream.of(makeDouble(line)))
                .toArray((String[][]::new));
    }
    public static String[][] makeDouble(String[] line) {
        String[] doubleArr = Arrays.stream(line)
                .flatMap(doublePoint -> Stream.of(doublePoint, doublePoint))
                .toArray(String[]::new);
        return new String[][]{doubleArr, doubleArr};
    }
}
// END
