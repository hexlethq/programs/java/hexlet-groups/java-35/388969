package exercise;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMqConfig {

    // BEGIN
    //Для создания очереди
    @Bean
    Queue queue() {
        return new Queue("queue", false);
    }

    //Обменник (помещает сообщения в определенную очередь)
    @Bean
    TopicExchange exchange() {
        return new TopicExchange("exchange");
    }

    //связывает очередь с разветвителем
    @Bean
    Binding binding(Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("key");
    }
    // END
}
