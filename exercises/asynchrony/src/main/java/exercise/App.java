package exercise;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.EnumSet;
import java.util.concurrent.CompletableFuture;

class App {

    // BEGIN
    private static String firstFile;

    private static String secondFile;

    private static String connectedFiles;
    // END

    public static void main(String[] args) throws Exception {
        // BEGIN
        App.unionFiles(
                "/main/resources/file1.txt",
                "src/main/resources/file2.txt",
                "src/main/resources/destPath.txt"
        );
        getDirectorySize("src/test/resources/dir");
        // END
    }

    public static CompletableFuture<String> unionFiles(String path1, String path2, String totalFile) throws NoSuchFileException {

        CompletableFuture<String> readFirstFile = CompletableFuture.supplyAsync(() -> {
            try {
                firstFile = Files.readString(Paths.get(path1));
            } catch (IOException e) {
                System.out.println(e);
            }
            return firstFile;
        });

        CompletableFuture<String> readSecondFile = CompletableFuture.supplyAsync(() -> {
            try {
                secondFile = Files.readString(Paths.get(path2));
            } catch (IOException e) {
                System.out.println(e);
            }
            return secondFile;
        });

        CompletableFuture<String> resultFile = readFirstFile.thenCombine(readSecondFile, (first, second) -> {
            if (!Files.exists(Paths.get(totalFile))) {
                try {
                    Files.createFile(Paths.get(totalFile));
                } catch (IOException e) {
                    System.out.println(e);
                }
            }
            try {
                Files.writeString(Paths.get(totalFile), firstFile + secondFile);
                connectedFiles = Files.readString(Paths.get(totalFile));
            } catch (IOException e) {
                System.out.println(e);
            }
            return connectedFiles;
        });

        return resultFile;
    }

    public static CompletableFuture<Long> getDirectorySize(String directory) {

        CompletableFuture<Long> resultSize = CompletableFuture.supplyAsync(() -> {
            try {
                EnumSet<FileVisitOption> opts = EnumSet.of(FileVisitOption.FOLLOW_LINKS);
                Finder finder = new Finder();

                Files.walkFileTree(Paths.get(directory), opts, 1, finder);

                System.out.println(Finder.findingSize);
                return Finder.findingSize;

            } catch (IOException e) {
                e.printStackTrace();
            }

            return 0L;
        });

        return resultSize;
    }

    private static class Finder extends SimpleFileVisitor {

        private static long findingSize = 0;

        @Override
        public FileVisitResult visitFile(Object file, BasicFileAttributes attrs) throws IOException {
            if (attrs.isDirectory())
                return FileVisitResult.SKIP_SUBTREE;
            findingSize += attrs.size();
            return FileVisitResult.CONTINUE;
        }
    }
}

