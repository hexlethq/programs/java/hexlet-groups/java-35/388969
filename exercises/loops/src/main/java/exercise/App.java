package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String phrase) {
        phrase = phrase.trim();
        StringBuilder firstLet = new StringBuilder("" + phrase.charAt(0));
        int len = phrase.length();
        for (int i = 1; i < len; i++) {
            if (phrase.charAt(i - 1) == ' ' && phrase.charAt(i) != ' ') {
                firstLet.append(phrase.charAt(i));
            }
        }
       return firstLet.toString().toUpperCase();
    }
    // END
}
