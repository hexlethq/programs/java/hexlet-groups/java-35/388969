package exercise;

// BEGIN
public class ReversedSequence implements CharSequence {
    private final String sentence;

    public ReversedSequence(String inSentence) {
        StringBuilder revSentence = new StringBuilder();
        char[] letters = inSentence.toCharArray();
        for (int i = letters.length -1; i != -1; i--) {
            revSentence.append(letters[i]);
        }
        this.sentence = revSentence.toString();
    }

    @Override
    public int length() {
        return this.sentence.length();
    }

    @Override
    public char charAt(int i) {
        return this.sentence.charAt(i - 1);
    }

    @Override
    public CharSequence subSequence(int i, int i1) {
        return this.sentence.substring(i, i1);
    }

    @Override
    public String toString() {
        return this.sentence;
    }
}
// END
