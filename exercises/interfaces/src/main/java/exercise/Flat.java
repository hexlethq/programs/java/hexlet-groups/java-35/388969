package exercise;

// BEGIN
public class Flat implements Home {

    private final double area;
    private final double balconyArea;
    private final int floor;

    public Flat(double area, double balconyArea, int floor) {
        this.area = area;
        this.balconyArea = balconyArea;
        this.floor = floor;
    }

    @Override
    public double getArea() {
        return this.area + this.balconyArea;
    }

    @Override
    public int compareTo(Home another) {
       if (getArea() == another.getArea()) {
           return 0;
       } else if (getArea() > another.getArea()) {
           return 1;
       }
       return -1;
    }

    @Override
    public String toString() {
        return String.format("Квартира площадью %s метров на %d этаже", getArea(), this.floor);
    }
}
// END
