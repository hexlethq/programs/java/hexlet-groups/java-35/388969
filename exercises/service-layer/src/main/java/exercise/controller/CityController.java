package exercise.controller;
import com.fasterxml.jackson.core.JsonProcessingException;
import exercise.CityNotFoundException;
import exercise.Weather;
import exercise.model.City;
import exercise.repository.CityRepository;
import exercise.service.WeatherService;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Map;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;


@RestController
public class CityController {

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private WeatherService weatherService;

    // BEGIN
    @GetMapping(path = "/cities/{id}")
    public Weather showWeatherInCurrentCity(@PathVariable long id) throws JsonProcessingException {
        City city = cityRepository.findById(id)
                .orElseThrow(() -> new CityNotFoundException("City does not exist"));

        return weatherService.getWeather(city.getName());
    }

    @GetMapping(path = "/search")
    public List<Map<String, Object>> getCitiesWithTemperature(
            @RequestParam(required = false) String name) {
        List<City> cities;

        if (StringUtils.isNotBlank(name)) {
            cities = cityRepository.findByNameContainingIgnoreCase(name);
            } else {
            cities = cityRepository.findByOrderByNameAsc();
        }

        List<Map<String, Object>> citiesWithTemperature = cities.stream()
                .map(city -> {
                    Map<String, Object> map = new TreeMap<>();

                    try {
                        map.put("temperature", weatherService.getWeather(city.getName()).getTemperature());
                    } catch (JsonProcessingException e) {
                        throw new CityNotFoundException("City does not exist");
                    }

                    map.put("name", city.getName());

                    return map;
                })
                .collect(Collectors.toList());

        return citiesWithTemperature;
    }
    // END
}

