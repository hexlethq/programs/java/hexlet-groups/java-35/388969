package exercise;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Weather {
    private String name;

    private int temperature;

    private String cloudy;

    private int wind;

    private int humidity;

    public Weather(){
    }

    public Weather(String name, int temperature, String cloudy, int wind, int humidity) {
        this.name = name;
        this.temperature = temperature;
        this.cloudy = cloudy;
        this.wind = wind;
        this.humidity = humidity;
    }
}
