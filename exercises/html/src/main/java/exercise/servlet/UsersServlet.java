package exercise.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import exercise.User;
import org.apache.commons.lang3.ArrayUtils;

public class UsersServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        String pathInfo = request.getPathInfo();

        if (pathInfo == null) {
            showUsers(request, response);
            return;
        }

        String[] pathParts = pathInfo.split("/");
        String id = ArrayUtils.get(pathParts, 1, "");

        showUser(request, response, id);
    }

    private List<User> getUsers() throws JsonProcessingException, IOException {
        // BEGIN
        List<User> users = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();

        users = mapper.readValue(new File("src/main/resources/users.json"),
                new TypeReference<List<User>>() {});

        return users;
        // END
    }

    private void showUsers(HttpServletRequest request,
                          HttpServletResponse response)
                throws IOException {

        // BEGIN
        List<User> users = getUsers();
        PrintWriter pw = response.getWriter();
        StringBuilder table = new StringBuilder();

        table.append("<table>");

        for (User user: users) {
            table.append("\n  <tr>");
            table.append("\n    <td>" + user.getId() + "</td>" + "\n    <td>");
            table.append("\n      <a href=\"/users/" + user.getId() + "\">");
            table.append(user.getFirstName() + " " + user.getLastName() + "</a>");
            table.append("\n    </td>");
            table.append("\n  </tr>");
        }

        table.append("</table>");

        pw.println(table);
        // END
    }

    private void showUser(HttpServletRequest request,
                         HttpServletResponse response,
                         String id)
                 throws IOException {

        // BEGIN
        List<User> users = getUsers();
        PrintWriter pw = response.getWriter();
        StringBuilder table = new StringBuilder();
        int checkUp = 0;

        if (!id.isBlank()) {

            for (User user : users) {

                if (user.getId().equals(id)) {
                    table.append("<style type=\"text/css\">"
                            + "\n TABLE {" + "\n  width: 400px;"
                            + "\n  border-collapse: collapse;" + "\n }"
                            + "\n TD, TH {" + "\n  padding: 3px;"
                            + "\n  border: 1px solid black;" + "\n }"
                            + "\n</style>\n");
                    table.append("<table>");
                    table.append("\n  <tr>");
                    table.append("\n    <td>id</td>");
                    table.append("\n    <td>First name</td>");
                    table.append("\n    <td>Last name</td>");
                    table.append("\n    <td>e-mail</td>");
                    table.append("\n  </tr>");
                    table.append("\n  <tr>");
                    table.append("\n    <td>" + user.getId() + "</td>");
                    table.append("\n    <td>" + user.getFirstName() + "</td>");
                    table.append("\n    <td>" + user.getLastName() + "</td>");
                    table.append("\n    <td>" + user.getEmail() + "</td>");
                    table.append("\n  </tr>");
                    table.append("\n</table>");

                    checkUp++;
                }
            }

            if (checkUp == 0) {
                response.sendError(404);
                pw.println("Not found");
            } else {
                pw.println(table);
            }

        } else {
            response.sendError(404);
            pw.println("Not found");
        }
        // END
    }
}
