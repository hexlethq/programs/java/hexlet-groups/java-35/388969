package exercise;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

// BEGIN
public class App {
    public static boolean scrabble(String simbols, String word) {
        boolean wordIsDone = false;
        int checkCompare = 0;
        if (!simbols.isEmpty() && !word.isEmpty()) {
            List<Character> letters = new ArrayList<>();
            int numberOfSimbols = simbols.length();
            for (int i = 0; i < numberOfSimbols; i++) {
                letters.add(simbols.charAt(i));
            }
            char[] lettersInWord = word.toLowerCase().toCharArray();
            for (char c : lettersInWord) {
                if (letters.contains(c)) {
                    letters.remove(Character.valueOf(c));
                    int noCompare = checkCompare;
                    checkCompare++;
                    if (checkCompare == noCompare) {
                        return wordIsDone;
                    }
                }
            }
            if (checkCompare == lettersInWord.length) {
                wordIsDone = true;
            }
        }
        return wordIsDone;
    }
}
//END
