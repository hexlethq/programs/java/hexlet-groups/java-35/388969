package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int sideA, int sideB, int sideC) {
        if (sideA + sideB < sideC) {
            return "Треугольник не существует";
        }
        if (sideA != sideB && sideA != sideC && sideC != sideB) {
            return "Разносторонний";
            }
        if (sideA == sideC && sideB == sideC) {
            return "Равносторонний";
                }
        return "Равнобедренный";
                }
    // END
}
