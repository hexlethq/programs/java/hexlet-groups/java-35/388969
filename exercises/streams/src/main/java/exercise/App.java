package exercise;

import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Arrays;
import java.util.stream.Collectors;

// BEGIN
class App {
    public static long getCountOfFreeEmails(List<String> emails) {
        return emails.stream()
                 .filter(StringUtils::isNotBlank)
                 .filter(name -> StringUtils.containsAny(name, "@yandex.ru", "@gmail.com", "@hotmail.com"))
                 .count();
    }
}

// END
