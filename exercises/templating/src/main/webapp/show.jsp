<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- BEGIN -->
<html>
  <head>
    <meta charset="UTF-8">
    <title>User</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"
    rel="stylesheet"
    integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We"
    crossorigin="anonymous">
  </head>
  <body>
       <tr>
         <td>id: ${user.get("id")}<br /></td>
         <td>Firstname: ${user.get("firstName")}<br /></td>
         <td>Lastname: ${user.get("lastName")}<br /></td>
         <td>email: ${user.get("email")}<br /></td>
         <td><a href='/users/delete?id=${user.get("id")}'>delete</a></td>
       </tr>
  </body>
</html>
<!-- END -->
