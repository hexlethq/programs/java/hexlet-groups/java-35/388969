package exercise;

import java.lang.reflect.Array;
import java.util.Arrays;

class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int[] arr) {
        int min = Integer.MIN_VALUE;
        int check = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < 0 && arr[i] > min) {
                    min = arr[i];
                    check = i;
                }
            }
        return check;
    }

    public static int[] getElementsLessAverage(int[] arr) {
        int[] fullArr = new int[arr.length];
        int miss = 0;
        double average = 0.0;
        for (int a: arr) {
            average += a;
        }
        for (int i = 0; i < arr.length; i++) {
            fullArr[i] = (arr[i] < average / arr.length ? arr[i] : miss++);
        }
        int[] res = Arrays.copyOf(fullArr, arr.length - miss);
        return res;
    }
    // END
    }
