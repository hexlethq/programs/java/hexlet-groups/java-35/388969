package exercise;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Map.Entry;

// BEGIN
public class App {
    public static List<Map<String, String>> findWhere(List<Map<String, String>> allBooks, Map<String, String> toFind) {
        List<Map<String, String>> resultOfSearch = new ArrayList<>();
        for (Map<String, String> partOfList: allBooks) {
            int howMuchCoincidence = 0;
            for (Map.Entry<String, String> compare: toFind.entrySet()) {
                if (partOfList.containsValue(compare.getValue())) {
                    howMuchCoincidence += 1;
                }
            }
            if (howMuchCoincidence == toFind.size()) {
                resultOfSearch.add(partOfList);
            }
        }
        return resultOfSearch;
    }
}
//END
