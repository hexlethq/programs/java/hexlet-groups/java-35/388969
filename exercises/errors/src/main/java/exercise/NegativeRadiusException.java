package exercise;

// BEGIN
public class NegativeRadiusException extends Exception{

    public NegativeRadiusException(String error) {
        super(error);
    }
}
// END
