package exercise;

import exercise.connections.Connection;
import exercise.connections.Disconnected;

import java.util.ArrayList;
import java.util.List;

// BEGIN
public class TcpConnection {
    private final String ip;
    private final int port;
    private Connection connection;

    public TcpConnection(String ip, int port) {
        this.ip = ip;
        this.port = port;
        this.connection = new Disconnected(this);
    }
    
    public void setCurrentState (Connection connect) {
        this.connection = connect;
    }

    public String getCurrentState() {
        return this.connection.getCurrentStatus();
    }

    public void connect() {
        this.connection.connect();
    }

    public void disconnect() {
        this.connection.disconnect();
    }

    public void write(String date) {
        this.connection.write(date);
    }
}
// END
