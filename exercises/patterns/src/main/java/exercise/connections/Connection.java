package exercise.connections;

public interface Connection {
    // BEGIN
    public void connect();
    public String getCurrentStatus();
    public void write(String data);
    public void disconnect();
    // END
}
