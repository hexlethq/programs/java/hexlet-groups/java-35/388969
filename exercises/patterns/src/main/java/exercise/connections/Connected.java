package exercise.connections;

import exercise.TcpConnection;

import java.util.ArrayList;

// BEGIN
public class Connected implements Connection {
    private TcpConnection tcpConnection;
    private ArrayList<String> buff = new ArrayList<>();

    public Connected(TcpConnection tcpConnection) {
        this.tcpConnection = tcpConnection;
    }

    @Override
    public void connect() {
        System.out.println("Error! Connection already connected");
    }

    @Override
    public String getCurrentStatus() {
        return "connected";
    }

    @Override
    public void write(String data) {
        this.buff.add(data);
    }

    @Override
    public void disconnect() {
        TcpConnection c = this.tcpConnection;
        c.setCurrentState(new Disconnected(c));
    }
}
// END
