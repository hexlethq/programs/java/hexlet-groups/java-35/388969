package exercise.connections;

import exercise.TcpConnection;

// BEGIN
public class Disconnected implements Connection {
    private TcpConnection tcpConnection;

    public Disconnected(TcpConnection tcpConnection) {
        this.tcpConnection = tcpConnection;
    }

    @Override
    public void connect() {
        TcpConnection c = this.tcpConnection;
        c.setCurrentState(new Connected(c));
    }

    @Override
    public String getCurrentStatus() {
        return "disconnected";
    }

    @Override
    public void write(String data) {
        System.out.println("Error! You need to set connect");
    }

    @Override
    public void disconnect() {
        System.out.println("Error! Connection already disconnected");
    }
}
// END
