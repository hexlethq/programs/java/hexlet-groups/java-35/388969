package exercise;

class App {
    // BEGIN
    public static int[] reverse(int[] arr) {
        int[] revArr = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
                revArr[arr.length - 1 - i] = arr[i];
        }
        return revArr;
    }
    public static int mult(int[] mas) {
        int fac = 1;
        for (int masVar : mas) {
            fac *= masVar;
        }
        return fac;
    }
    // END
}
