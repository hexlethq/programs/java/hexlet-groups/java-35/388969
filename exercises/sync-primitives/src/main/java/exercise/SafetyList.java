package exercise;

class SafetyList {
    // BEGIN
    private int size = 0;

    private int[] arr = new int[1];

    public SafetyList() {
    }

    public synchronized void add(int number) {
        if (size == arr.length) {
            final int[] oldArr = arr;
            arr = new int[size * 2];
            System.arraycopy(oldArr, 0, arr, 0, size);
        }
        arr[size++] = number;
    }

    public int get(int index) {
        return arr[index];
    }

    public int getSize() {
        return size;
    }
    // END
}
