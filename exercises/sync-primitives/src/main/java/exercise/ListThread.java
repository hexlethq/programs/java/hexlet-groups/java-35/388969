package exercise;

import java.util.Random;

// BEGIN
public class ListThread extends Thread{

    private SafetyList safetyList;

    public ListThread(SafetyList newSafeList) {
        this.safetyList = newSafeList;
    }

    @Override
    public void run() {
        Random random = new Random();

        for (int i = 0; i < 1000; i++) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            safetyList.add(random.nextInt());
        }
    }
}
// END
