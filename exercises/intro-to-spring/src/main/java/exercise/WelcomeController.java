package exercise;

import org.springframework.web.bind.annotation.*;

// BEGIN
@RestController
public class WelcomeController {
    @GetMapping("/")
    public String getWelcome() {
        return "Welcome to Spring";
    }

    @GetMapping("/hello")
    @ResponseBody
    public String getHello(@RequestParam(defaultValue = "World") String name) {
        return "Hello, " + name + "!";
    }
}
// END
