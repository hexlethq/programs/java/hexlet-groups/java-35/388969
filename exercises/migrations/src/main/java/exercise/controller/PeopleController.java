package exercise.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/people")
public class PeopleController {
    @Autowired
    JdbcTemplate jdbc;

    @PostMapping(path = "")
    public void createPerson(@RequestBody Map<String, Object> person) {
        String query = "INSERT INTO person (first_name, last_name) VALUES (?, ?)";
        jdbc.update(query, person.get("first_name"), person.get("last_name"));
    }

    // BEGIN
    @GetMapping(path = "", produces = "application/json")
    @ResponseBody
    public String allPersons() throws JsonProcessingException {
        String query = "SELECT (first_name, last_name) FROM person";
        List<Map<String, Object>> persons = jdbc.queryForList(query);

        ObjectMapper objectMapper = new ObjectMapper();
        StringBuilder personsJson = new StringBuilder();

        for (Map<String, Object> person: persons) {
            personsJson.append(objectMapper.writeValueAsString(person));
        }

        return personsJson.toString();
    }

    @GetMapping(path = "/{id}", produces = "application/json")
    @ResponseBody
    public String showPerson(@PathVariable int id) throws JsonProcessingException {
        String query = "SELECT (first_name, last_name) FROM person WHERE id = (?)";
        Map<String, Object> person = jdbc.queryForMap(query, id);

        ObjectMapper objectMapper = new ObjectMapper();

        return objectMapper.writeValueAsString(person);
    }
    // END
}
