package exercise;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// BEGIN
class App {
    public static LinkedHashMap<String, String> genDiff(Map<String, Object> firstMap, Map<String, Object> secondMap) {

        Set<String> keys = new TreeSet<>(firstMap.keySet());
        keys.addAll(secondMap.keySet());

        return keys.stream()
                .collect(Collectors.toMap(k -> k, n -> {
                    if (firstMap.containsKey(n) && secondMap.containsKey(n)) {
                        if (firstMap.containsValue(secondMap.get(n))) {
                            return "unchanged";
                        } else {
                            return "changed";
                        }
                    }
                    if (!firstMap.containsKey(n) && secondMap.containsKey(n)) {
                        return  "added";
                    }
                    if (firstMap.containsKey(n) && !secondMap.containsKey(n)) {
                       return  "deleted";
                    }
                    return null;
                }, (k1, k2) -> k1, LinkedHashMap::new));
    }
}
//END
