package exercise.controller;

import exercise.model.Course;
import exercise.repository.CourseRepository;
import liquibase.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/courses")
public class CourseController {

    @Autowired
    private CourseRepository courseRepository;

    @GetMapping(path = "")
    public Iterable<Course> getCorses() {
        return courseRepository.findAll();
    }

    @GetMapping(path = "/{id}")
    public Course getCourse(@PathVariable long id) {
        return courseRepository.findById(id);
    }

    // BEGIN
    @GetMapping("/{id}/previous")
    public List<Course> getParents(@PathVariable long id) {
        Course course = courseRepository.findById(id);
        String path = course.getPath();

        if (StringUtil.isEmpty(path)) {
            return new ArrayList<>();
        }

        String[] parentsId = path.split("\\.");
        return Arrays.stream(parentsId)
                .map(parentId -> courseRepository.findById(Long.parseLong(parentId)))
                .collect(Collectors.toList());
    }
    // END

}
