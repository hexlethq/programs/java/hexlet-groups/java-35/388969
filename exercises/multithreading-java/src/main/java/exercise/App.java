package exercise;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.logging.Level;




class App {
    private static final Logger LOGGER = Logger.getLogger("AppLogger");

    // BEGIN
    public static void main(String[] args) {
        int[] numbers = {10, -4, 67, 12, -238, 8};
        System.out.println(getMinMax(numbers));
    }

    public static Map<String, Integer> getMinMax(int[] array) {
        Map<String, Integer> result = new HashMap<>();

        MaxThread maxThread = new MaxThread(array);
        MinThread minThread = new MinThread(array);

        maxThread.start();
        LOGGER.info(maxThread.getName() + " started");

        minThread.start();
        LOGGER.info(minThread.getName() + " started");

        try {
            maxThread.join();
        } catch (InterruptedException e) {
            System.out.println("поток максимума был прерван");
        }

        try {
            minThread.join();
        } catch (InterruptedException e) {
            System.out.println("поток минимума был прерван");;
        }

        result.put("max", maxThread.getMax());
        LOGGER.info(maxThread.getName() + " finished");

        result.put("min", minThread.getMin());
        LOGGER.info(minThread.getName() + " finished");

        return result;
    }
    // END
}

