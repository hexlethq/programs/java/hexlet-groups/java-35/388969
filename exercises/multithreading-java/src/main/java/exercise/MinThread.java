package exercise;

import java.util.Arrays;
import java.util.stream.IntStream;

// BEGIN
public class MinThread extends Thread {

    private int[] arr;

    private int min;

    public MinThread(int[] newArr) {
        this.arr = Arrays.stream(newArr).toArray();
    }

    public int getMin() {
        return min;
    }

    @Override
    public void run() {
        this.min = Arrays.stream(arr)
                .min().getAsInt();
    }
}
// END
