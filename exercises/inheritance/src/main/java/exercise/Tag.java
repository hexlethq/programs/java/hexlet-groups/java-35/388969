package exercise;

import java.util.Map;

// BEGIN
public abstract class Tag {
    protected final String tag;
    protected final Map<String, String> attributes;
    protected final StringBuilder textVision = new StringBuilder();

    protected Tag(String tag, Map<String, String> attributes) {
        this.tag = tag;
        this.attributes = attributes;
    }

    public String toString() {
        textVision.append("<" + tag);

        for (Map.Entry attribute: attributes.entrySet()) {
            textVision.append(" " + attribute.getKey() + "=");
            textVision.append("\"" + attribute.getValue() + "\"");
        }

        textVision.append(">");

        return textVision.toString();
    }
}
// END
