package exercise;

import java.util.Map;
import java.util.List;

// BEGIN
public class PairedTag extends Tag{
    private final String tagBody;
    private final List<Tag> children;

    protected PairedTag(String tag, Map<String, String> attributes,
                        String tagBody, List<Tag> children) {

        super(tag, attributes);

        this.tagBody = tagBody;
        this.children = children;
    }

    @Override
    public String toString() {
        super.toString();

        if (!children.isEmpty()) {

            for (Tag child: children) {
                textVision.append(child.toString());
            }

        } else {
            textVision.append(tagBody);
        }

        textVision.append("</" + tag + ">");

        return textVision.toString();
    }
}
// END
