package exercise;

import java.util.Map;

// BEGIN
public class SingleTag extends Tag{

    protected SingleTag(String tag, Map<String, String> attributes) {
        super(tag, attributes);
    }
}
// END
