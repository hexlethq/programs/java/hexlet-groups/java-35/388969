package exercise;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

class App {
    // BEGIN
    public static String buildList(String[] arr) {
        StringBuilder list = new StringBuilder();
        if (arr.length > 0) {
            list.append("<ul>\n");
            for (String s : arr) {
                list.append("  ").append("<li>").append(s).append("</li>").append("\n");
            }
            list.append("</ul>");
        }
        return list.toString();
    }

    public static String getUsersByYear(String[][] usr, int year) {
        StringBuilder res = new StringBuilder();
        boolean check = false;
        String yer = Integer.toString(year);
            for (int i = 0; i < usr.length; i++) {
                String us = usr[i][1].substring(0, 4);
                if (us.equals(yer)) {
                    res.append("  ").append("<li>").append(usr[i][0]).append("</li>").append("\n");
                    check = true;
                }
            }
            if (check) {
                res.insert(0, "<ul>\n");
                res.append("</ul>");
            }
        return res.toString();
    }
    // END
/*
    // Это дополнительная задача, которая выполняется по желанию.
    public static String getYoungestUser(String[][] users, String date) throws Exception {
        // BEGIN
        
        // END
    }

 */
}
