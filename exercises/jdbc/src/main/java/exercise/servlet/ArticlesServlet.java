package exercise.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletContext;

import java.util.*;

import org.apache.commons.lang3.ArrayUtils;

import exercise.TemplateEngineUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;



public class ArticlesServlet extends HttpServlet {

    private String getId(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            return null;
        }
        String[] pathParts = pathInfo.split("/");
        return ArrayUtils.get(pathParts, 1, null);
    }

    private String getAction(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            return "list";
        }
        String[] pathParts = pathInfo.split("/");
        return ArrayUtils.get(pathParts, 2, getId(request));
    }

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
                throws IOException, ServletException {

        String action = getAction(request);

        switch (action) {
            case "list":
                showArticles(request, response);
                break;
            default:
                showArticle(request, response);
                break;
        }
    }

    private void showArticles(HttpServletRequest request,
                          HttpServletResponse response)
            throws IOException, ServletException {

        ServletContext context = request.getServletContext();
        Connection connection = (Connection) context.getAttribute("dbConnection");
        // BEGIN
        PreparedStatement preparedStatement = null;
        List<Map<String, String>> articles = new ArrayList<>();
        int page = 1;

        if (request.getParameter("page") != null) {
            page = Integer.parseInt(request.getParameter("page"));
        }

        try {
            preparedStatement = connection.prepareStatement("SELECT * FROM ARTICLES ORDER BY id LIMIT 10 OFFSET ?");
            preparedStatement.setInt(1, (page - 1) * 10);
            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                articles.add(Map.of(
                        "id", result.getString("id"),
                        "title", result.getString("title"),
                        "body", result.getString("body")
                        )
                );
            }


        } catch (SQLException e) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return;
        }

        request.setAttribute("articles", articles);
        request.setAttribute("prePage", (page - 1));
        request.setAttribute("nextPage", (page + 1));

        // END
        TemplateEngineUtil.render("articles/index.html", request, response);
    }

    private void showArticle(HttpServletRequest request,
                         HttpServletResponse response)
                 throws IOException, ServletException {

        ServletContext context = request.getServletContext();
        Connection connection = (Connection) context.getAttribute("dbConnection");
        // BEGIN
        String pathInfo = request.getPathInfo();
        String[] pathParts = pathInfo.split("/");
        String titleId = pathParts[1];

        PreparedStatement preparedStatement = null;
        Map<String, String> article = new HashMap<>();

        try {
            preparedStatement = connection.prepareStatement("SELECT * FROM ARTICLES WHERE id = ?");
            preparedStatement.setInt(1, Integer.parseInt(titleId));
            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                if(result.getString("id").equals(titleId)) {
                    article.put("id", result.getString("id"));
                    article.put("title", result.getString("title"));
                    article.put("body", result.getString("body"));
                }
            }

        } catch (SQLException e) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Something goes wrong :(");
            return;
        }

        if (article.isEmpty()) {
            response.sendError(404);
        }

        request.setAttribute("article", article);
        // END
        TemplateEngineUtil.render("articles/show.html", request, response);
    }
}
