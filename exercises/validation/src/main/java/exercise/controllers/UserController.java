package exercise.controllers;

import io.javalin.http.Handler;
import java.util.List;
import java.util.Map;
import io.javalin.core.validation.Validator;
import io.javalin.core.validation.ValidationError;
import io.javalin.core.validation.JavalinValidation;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.lang3.StringUtils;

import exercise.domain.User;
import exercise.domain.query.QUser;

public final class UserController {

    public static Handler listUsers = ctx -> {

        List<User> users = new QUser()
            .orderBy()
                .id.asc()
            .findList();

        ctx.attribute("users", users);
        ctx.render("users/index.html");
    };

    public static Handler showUser = ctx -> {
        long id = ctx.pathParamAsClass("id", Long.class).getOrDefault(null);

        User user = new QUser()
            .id.equalTo(id)
            .findOne();

        ctx.attribute("user", user);
        ctx.render("users/show.html");
    };

    public static Handler newUser = ctx -> {

        ctx.attribute("errors", Map.of());
        ctx.attribute("user", Map.of());
        ctx.render("users/new.html");
    };

    public static Handler createUser = ctx -> {
        // BEGIN
        String inputFirstName = ctx.formParam("firstName");
        String inputLastName = ctx.formParam("lastName");
        String inputPassword = ctx.formParam("password");
        String inputEmail = ctx.formParam("email");

        Validator<String> firstNameValidator = ctx.formParamAsClass("firstName", String.class)
                .check(name -> !name.isBlank(),  "Имя не должно быть пустым");
        Validator<String> lastNameValidator = ctx.formParamAsClass("lastName", String.class)
                .check(name -> !name.isBlank(),  "Имя не должно быть пустым");
        Validator<String> emailValidator = ctx.formParamAsClass("email", String.class)
                .check(email -> EmailValidator.getInstance().isValid(email),  "Неправильно указан email");
        Validator<String> passwordValidator = ctx.formParamAsClass("password", String.class)
                .check(password -> password.length() > 4,  "Пароль должен быть не короче 4 символов")
                .check(StringUtils::isNumeric, "Пароль должен содержать только цифры");

        Map<String, List<ValidationError<? extends Object>>> errors = JavalinValidation.collectErrors(
                firstNameValidator,
                lastNameValidator,
                emailValidator,
                passwordValidator
        );

        if (!errors.isEmpty()) {
            ctx.status(422);
            ctx.attribute("errors", errors);
            User invalidUser = new User(inputFirstName, inputLastName, inputEmail, inputPassword);
            ctx.attribute("user", invalidUser);
            ctx.render("users/new.html");
            return;
        }

        User user = new User(inputFirstName, inputLastName, inputEmail, inputPassword);
        user.save();

        ctx.sessionAttribute("flash", "Пользователь создан");
        ctx.redirect("/users");
        // END
    };
}
