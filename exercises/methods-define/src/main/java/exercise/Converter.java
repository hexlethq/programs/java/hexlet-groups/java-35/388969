package exercise;

class Converter {
    // BEGIN
    public static int convert(int quantity, String size) {
        int result;
        if (size.equals("Kb")) {
            result = quantity / 1024;
        }
        else {
            if (size.equals("b")) {
               result = quantity * 1024;
            }
            else {
               result = 0;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println("10 Kb = " + convert(10, "b") + " b");
    }
    // END
}
