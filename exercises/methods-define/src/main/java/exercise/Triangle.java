package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(int sideA, int sideB, int corner) {
        double radCorner = corner * 3.14 / 180;
        return (sideA * sideB * Math.sin(radCorner)) / 2;
    }

    public static void main(String[] args) {
        System.out.println(4 * 5 * Math.sin(45 * 3.14 / 180) / 2);
    }
    // END
}
