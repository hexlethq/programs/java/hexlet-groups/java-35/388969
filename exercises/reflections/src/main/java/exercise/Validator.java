package exercise;


import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

// BEGIN
public class Validator {
    public static List<String> validate(Address address) {
        List<String> valueNull = new LinkedList<>();

        for (Field field: address.getClass().getDeclaredFields()) {

            NotNull notNull = field.getAnnotation(NotNull.class);
            field.setAccessible(true);

            try {
                if (notNull != null && field.get(address) == null) {
                    valueNull.add(field.toString().substring(field.toString().lastIndexOf(".") + 1));
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return valueNull;
    }

    public static Map<String, List<String>> advancedValidate(Address address) {
        Map<String, List<String>> notValidate = new HashMap<>();

        for (Field field: address.getClass().getDeclaredFields()) {

            NotNull notNull = field.getAnnotation(NotNull.class);
            MinLength minLength = field.getAnnotation(MinLength.class);

            List<String> report = new LinkedList<>();

            field.setAccessible(true);

            try {

                if (notNull != null && field.get(address) == null)
                    report.add("can not be null");

                if (minLength != null && (field.get(address) == null || field.get(address).toString().length() < minLength.minLength()))
                    report.add("length less than " + minLength.minLength());

                if (!report.isEmpty())
                    notValidate.put(field.toString().substring(field.toString().lastIndexOf(".") + 1), report);

            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return notValidate;
    }
}
// END
