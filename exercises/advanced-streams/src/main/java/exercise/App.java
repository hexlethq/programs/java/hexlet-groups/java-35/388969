package exercise;

import com.sun.source.doctree.SeeTree;

import java.util.stream.Collectors;
import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
class App {
    public static String getForwardedVariables(String options) {
        return Stream.of(options)
                .map(option -> option.split("\n"))
                .flatMap(Stream::of)

                .filter(option -> option.startsWith("environment"))
                .map(option -> option.split(","))
                .flatMap(Stream::of)

                .filter(option -> option.contains("X_FORWARDED_"))
                .map(option -> option.replaceAll(".+_FORWARDED_", ""))
                .map(option -> option.replaceAll("\"", ""))
                .collect(Collectors.joining(","));
    }
}
//END
