package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
class FileKV implements KeyValueStorage {

    private final String filePath;
    private final Map<String, String> storage = new HashMap<>();

    public FileKV(String filePath, Map<String, String> storage) {
        this.filePath = filePath;
        this.storage.putAll(storage);
        Utils.writeFile(filePath, Utils.serialize(storage));
    }

    @Override
    public void set(String key, String value) {
        storage.put(key, value);
        String json = Utils.serialize(storage);
        Utils.writeFile(filePath, json);
    }

    @Override
    public void unset(String key) {
        storage.remove(key);
        String json = Utils.serialize(storage);
        Utils.writeFile(filePath, json);
    }

    @Override
    public String get(String key, String defaultValue) {
        String json = Utils.readFile(filePath);
        Map<String, String> result = Utils.unserialize(json);
        return result.getOrDefault(key, defaultValue);
    }

    @Override
    public Map<String, String> toMap() {
        String json = Utils.readFile(filePath);
        return Utils.unserialize(json);
    }
}
// END
