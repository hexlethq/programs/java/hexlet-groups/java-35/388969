package exercise;

import java.util.Map;

interface KeyValueStorage {
    //добавляет в словарь новое значение по указанному ключу
    void set(String key, String value);
    //удаляет из словаря значение по переданному ключу
    void unset(String key);
    /*
    Возвращает значение по указанному ключу
    Если такого ключа в словаре нет, возвращает значение по умолчанию
    */
    String get(String key, String defaultValue);
    //возвращает базу данных в виде словаря Map
    Map<String, String> toMap();
}
