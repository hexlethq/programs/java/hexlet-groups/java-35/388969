package exercise;

import java.util.Map;

// BEGIN
public class App {
    //Метод принимает на вход объект базы данных (KeyValueStorage) и меняет в нём ключи и значения местами
    public static KeyValueStorage swapKeyValue(KeyValueStorage inMap) {
        Map<String, String> outMap = inMap.toMap();
        for (Map.Entry oneSwap:  outMap.entrySet()) {
            inMap.unset((String) oneSwap.getKey());
            inMap.set((String) oneSwap.getValue(), (String) oneSwap.getKey());
        }
        return inMap;
    }
}
// END
